//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectDepthFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public string EmpID { get; set; }
        public string EmpCode { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public Nullable<byte> Job_level { get; set; }
        public string Department { get; set; }
        public Nullable<byte> Status { get; set; }
        public string ManagerID { get; set; }
    }
}
